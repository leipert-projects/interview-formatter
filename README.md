# What is this?

I have developed this tool for taking notes during an interview.
Our candidate platform has no rich-text capabilities, but I wanted to leave human readable notes and summaries.

It takes a markdown input and outputs it in plain-text, word wrapped at 80 characters. Paragraphs are indented.

# What do we support?

Simple paragraphs

1. Ordered
1. lists

- bullet
- points

```bash
# code blocks
```
