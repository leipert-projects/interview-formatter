export default async function htmlToMd(text) {
  const [prettier, prettierMarkdown, TurndownService, DOMPurify] = (
    await Promise.all([
      import('prettier/esm/standalone.mjs'),
      import('prettier/esm/parser-markdown.mjs'),
      import('turndown'),
      import('dompurify'),
    ])
  ).map((x) => x.default)

  const turndownService = new TurndownService({
    headingStyle: 'atx',
    bulletListMarker: '-',
  })

  const div = document.createElement('div')

  div.innerHTML = DOMPurify.sanitize(text)

  div.querySelectorAll('span').forEach((node) => {
    const { fontWeight = '400', fontStyle = 'normal' } = node.style
    let replaced = false
    if (parseInt(fontWeight, 10) > 400) {
      const rep = document.createElement('strong')
      rep.innerHTML = node.innerHTML
      node.replaceWith(rep)
      replaced = true
    }
    if (fontStyle === 'italic') {
      const rep = document.createElement('em')
      rep.innerHTML = replaced ? node.outerHTML : node.innerHTML
      node.replaceWith(rep)
    }
  })

  return prettier.format(turndownService.turndown(div.innerHTML), {
    plugins: [prettierMarkdown],
    parser: 'markdown',
  })
}
