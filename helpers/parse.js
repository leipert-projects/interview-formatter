const markdownIt = require('markdown-it')()

const ORDERED = 'ordered'
const UNORDERED = 'unordered'
const MAX_LENGTH = 80
const PADDING = 6

export const WORKDAY = 'WORKDAY'
export const GREENHOUSE = 'GREENHOUSE'

const MODES = {
  [WORKDAY]: { whitespace: ' ', paragraph: '\n\n\n' },
  [GREENHOUSE]: { whitespace: '\u2002', paragraph: '\n\n' },
}

export default function parse(input, mode = GREENHOUSE) {
  const result = []

  const { whitespace, paragraph } = MODES[mode]

  const parsed = markdownIt.parse(input, {})

  const linkMap = {}

  let curr = []

  let listType = null

  const wordWrap =
    (offset = 0) =>
    (string) => {
      const maxLen = MAX_LENGTH - offset
      if (string?.length < maxLen) {
        return [string]
      }
      const split = string.split(' ')
      let curr = split[0]
      const res = []
      for (let i = 1; i < split.length; i += 1) {
        if (curr.length + split[i].length + 1 > maxLen) {
          res.push(curr)
          curr = split[i]
        } else {
          curr += ' ' + split[i]
        }
      }
      res.push(curr)
      return res
    }

  const split = (x) => x.split(/\n/)

  const close = () => {
    const res = [...curr]
    curr = []
    return res
  }

  const padLeft = (length) => (x) => ''.padStart(length, whitespace) + x

  const closeParagraph = () => {
    result.push(
      close().flatMap(split).flatMap(wordWrap(PADDING)).map(padLeft(PADDING))
    )
  }

  const closeBulletList = () => {
    result.push(
      close()
        .map((item) =>
          split(item)
            .flatMap(wordWrap(PADDING))
            .map((x, i) => {
              if (i === 0) {
                return padLeft(PADDING - 2)(`- ${x}`)
              }
              return padLeft(PADDING)(x)
            })
        )
        .flat()
    )
  }

  const closeOrderedList = () => {
    result.push(
      close()
        .map((item, index) =>
          split(item)
            .flatMap(wordWrap(PADDING))
            .map((x, i) => {
              if (i === 0) {
                const label = `${index + 1}. `
                return padLeft(PADDING - label.length)(label) + x
              }
              return padLeft(PADDING)(x)
            })
        )
        .flat()
    )
  }

  const closeHeading = () => {
    result.push(
      close()
        .flatMap(split)
        .flatMap(wordWrap(PADDING / 2))
        .map((x, i) => {
          if (i === 0) {
            return `# ${x}`
          }
          return padLeft(PADDING / 2)(x)
        })
    )
  }

  const pushContent = (content) => {
    if (content) {
      curr.push(content.replace(/=>/g, '⇒'))
    }
  }

  const renderInline = (inline) => {
    const result = []
    const children = inline?.children || []
    let currLink = null
    for (let i = 0; i < children.length; i += 1) {
      const token = children[i]
      switch (token?.type) {
        case 'image':
          result.push('[image not supported]')
          break
        case 'code_inline':
          result.push(`\`${token.content}\``)
          break
        case 'text':
          result.push(token.content)
          break
        case 'hardbreak':
          result.push('\n')
          break
        case 'softbreak':
          result.push(' ')
          break
        case 'link_close':
          if (currLink) {
            result.push(`][${linkMap[currLink]}]`)
          }
          currLink = 0
          break
        case 's_open':
        case 's_close':
        case 'em_open':
        case 'em_close':
        case 'strong_open':
        case 'strong_close':
          result.push(token.markup)
          break
        case 'link_open':
          currLink = token?.attrs?.find(([name, val]) => name === 'href')[1]
          if (currLink && !linkMap[currLink]) {
            linkMap[currLink] = Object.values(linkMap).length + 1
          }
          result.push('[')
          break
        default:
          if (token) {
            result.push([
              'Unsupported markdown found' + JSON.stringify(token, null, 2),
            ])
          }
      }
    }
    return result.join('')
  }

  for (let i = 0; i <= parsed.length; i += 1) {
    const token = parsed[i]
    switch (token?.type) {
      case 'bullet_list_open':
        listType = UNORDERED
        break
      case 'ordered_list_open':
        listType = ORDERED
        break
      case 'list_item_open':
        break
      case 'heading_open':
        break
      case 'heading_close':
        closeHeading()
        break
      case 'paragraph_close':
      case 'blockquote_close':
        if (!listType) {
          closeParagraph()
        }
        break
      case 'ordered_list_close':
        listType = null
        closeOrderedList()
        break
      case 'bullet_list_close':
        listType = null
        closeBulletList()
        break
      case 'inline':
        pushContent(renderInline(token))
        break
      case 'paragraph_open':
      case 'blockquote_open':
      case 'list_item_close':
        break
      case 'hr':
        pushContent('- - -')
        closeParagraph()
        break
      case 'fence':
      case 'code_block':
        pushContent('```' + token?.info)
        pushContent((token?.content || '').trim())
        pushContent('```')
        closeParagraph()
        break
      default:
        if (token) {
          result.push([
            'Unsupported markdown found' + JSON.stringify(token, null, 2),
          ])
        }
    }
  }

  const linkMapped = []

  Object.entries(linkMap).forEach(([url, idx]) => {
    linkMapped.push(`[${idx}]: ${url}`)
  })

  if (linkMapped.length) {
    result.push(linkMapped)
  }

  return result.map((x) => x.join('\n')).join(paragraph)
}
